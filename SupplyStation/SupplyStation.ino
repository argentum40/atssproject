/*
 Name:		SupplyStation.ino
 Created:	8/30/2017 7:19:08 PM
 Author:	rvenancio2
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

float flow; //Vari�vel para armazenar o valor em L/min
float media = 0; //Vari�vel para fazer a m�dia
int countPulse; //Vari�vel para a quantidade de pulsos
int i = 0; //Vari�vel para segundos
int Min = 00; //Vari�vel para minutos
float Litros = 0; //Vari�vel para Quantidade de agua
float MiliLitros = 0; //Variavel para Convers�o

uint8_t flowMetterInput = D0;

uint8_t pressureTransmitterInput = A0;
float pressure;
uint8_t pump1Output = D1;


/* Create a WiFi access point and provide a web server on it. */


/* Set these to your desired credentials. */
const char *ssid = "ESPap";
const char *password = "there";

//using softip from router
ESP8266WebServer server(80);


// the setup function runs once when you press reset or power the board
void setup() {

	pinMode(flowMetterInput, INPUT);
	attachInterrupt(0, incPulse, RISING); //Configura o pino 2(Interrup��o 0) interrup��o
	pinMode(pressureTransmitterInput, INPUT);

	delay(1000);
	Serial.begin(115200);
	Serial.println();
	Serial.print("Configuring access point...");
	/* You can remove the password parameter if you want the AP to be open. */
	WiFi.softAP(ssid, password);

	IPAddress myIP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(myIP);

	// Start the server
	server.on("/", handleRoot);
	server.on("/clientStation", handleClientStation);

	server.begin();
	Serial.println("HTTP server started");

}

// the loop function runs over and over again until power down or reset
void loop() {
	server.handleClient();	
}

/* Just a little test message.  Go to http://192.168.4.1 in a web browser
* connected to this access point to see it.
*/
void handleRoot() {
	server.send(200, "text/html", "<h1>You are connected</h1>");

}

/* Just a little test message.  Go to http://192.168.4.1/clientStation in a web browser
* connected to this access point to see it.
	every time, the client request this url. 
	remenber; is needed id the client
*/
void handleClientStation() {
	String message = "";
	char arg;
	if (server.arg("arg") == "") {     //Parameter not found
		message = "Arg Argument not found";
	}
	else {     //Parameter found
		message = "Arg Argument = ";
		message += server.arg("arg");     //Gets the value of the query parameter
		//aqui pegamos o arg que � uma string e convertemos pra char array e pegamos a pos 0.
		char status = server.arg("arg").toCharArray[0];
		
		//precisa identificar qual foi a estacao que enviou isso.
		 
		//precisa tratar 3 casos
		
		// se nivel alto na esta��o. mandar fechar a valvula e/ou parar a bomba caso nao tenha outra estacao recebendo agua.
		// se nivel baixo, manda ligar a valvula da esta��o.
		// se nivel baixo e j� esta com a valvula aberta. manda ligar a bomba.

		
		Serial.println(arg);
	}
	server.send(200, "text/html", message);       //Response to the HTTP request
}


void incPulse(){
	countPulse++; //Incrementa a vari�vel de pulsos
}

void contaVazao(){
	countPulse = 0;//Zera a vari�vel
	sei(); //Habilita interrup��o
	delay(1000); //Aguarda 1 segundo
	cli(); //Desabilita interrup��o

	flow = countPulse / 5.5; //Converte para L/min
	media = media + flow; //Soma a vaz�o para o calculo da media
	i++;
	Serial.print(flow); //Escreve no display o valor da vaz�o
	Serial.print(" L/min "); //Escreve L/min
	Serial.print(Min);
	Serial.print(":"); //Escreve :
	Serial.print(i); //Escreve a contagem i (segundos)
	Serial.print("Min "); //Escreve :
	MiliLitros = flow / 60;
	Litros = Litros + MiliLitros;
	Serial.print(Litros);
	Serial.print("L ");

	// Neste conjunto de linhas fizemos a m�dia das leituras obtidas a cada 1 minuto
	if (i == 60)
	{
		Min++;
		Serial.print(Min);
		if (Min >= 60)
		{
			Min = 0;
		}
		media = media / 60; //faz a m�dia
		Serial.print("nMedia por minuto = "); //Imprime a frase Media por minuto =
		Serial.print(media); //Imprime o valor da media
		Serial.println(" L/min - "); //Imprime L/min
		media = 0; //Zera a vari�vel media para uma nova contagem
		i = 0; //Zera a vari�vel i para uma nova contagem	
}
}

//convert analog pressure sensor to value in bar
float getConvertedPressureInput(){	
	int rawADC = analogRead(pressureTransmitterInput);
	float volt = (float)rawADC * 5.0 / 1024.0;
	float bar = (volt - 0.1) / 4.5 * 7.14;   // minus offset, divide by 
											//voltage range, multiply with pressure range.
	return bar;
}


byte BoolArrayToByte(bool boolArray[8])
{

}

unsigned char ToByte(bool b[8])
{

}

void FromByte(unsigned char c, bool b[8])
{
	
}
