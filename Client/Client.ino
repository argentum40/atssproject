/*
 Name:		Client.ino
 Created:	8/30/2017 7:18:25 PM
 Author:	rvenancio2
*/

#include <ESP8266WiFi.h>

//const char* ssid = "RACENET-RODRIGO-83381991";
//const char* password = "30062014";
const char *ssid = "RemoteControl";
const char *password = "remotecontrol";

const char* host = "192.168.4.1";

void setup() {
	Serial.begin(115200);
	delay(10);

	// We start by connecting to a WiFi network

	Serial.println();
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(ssid);

	/* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
	would try to act as both a client and an access-point and could cause
	network-issues with your other WiFi-devices on your WiFi-network. */
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
	bool statusStation[8];
}

int value = 0;

void loop() {
	delay(5000);
	++value;

	Serial.print("connecting to ");
	Serial.println(host);


	// o programa cliente fica aqui.
	// primeiro ler todas as entradas e atualizar em status station.
	
	//em duas ocasioes que enviamos dados para o servidor.
	//1 -quando der nivel alto
	//2 - quando estiver nivel baixo 
	
	//usar um if para fazer isso se necessario.
	char sts = ToByte(statusStation);
	sendStatusToServer(sts);

	// onde sera enviada uma estrutura que representa o status deste cliente. o servidor ir� responder com a a��o a ser comandada.
	
	//  0  1  2  3  4  5  6  7  
	// [0][0][0][0][0][0][0][1] - vetor de booleanos
	//	
	//cada um vai representar algo. nivel baixo, nivel alto, cmd valvula, status valvula, status bomba e etc.

}

//falta descomentar a linha dentro
void sendStatusToServer(String sts)
{
	// Use WiFiClient class to create TCP connections
	WiFiClient client;
	const int httpPort = 80;
	if (!client.connect(host, httpPort)) {
		Serial.println("connection failed");
		return;
	}
	
	//clientStation?arg=32
	// We now create a URI for the request
	String path = "/clientStation";
	String param = "arg";
	
	//ta mandando valor constante. se quiser mudar comenta uma e descomenta outra.
	String value = "32";
	//value = sts;


	String url = path;
	url += "?";
	url += param;
	url += "=";
	url += value;;


	Serial.print("Requesting URL: ");
	Serial.println(url);

	// This will send the request to the server
	client.print(String("GET ") + url + " HTTP/1.1\r\n" +
		"Host: " + host + "\r\n" +
		"Connection: close\r\n\r\n");
	delay(1000);
	unsigned long timeout = millis();
	while (client.available() == 0) {
		if (millis() - timeout > 5000) {
			Serial.println(">>> Client Timeout !");
			client.stop();
			return;
		}
	}

	// Read all the lines of the reply from server and print them to Serial
	while (client.available()) {
		//aqui ele ta lendo o que o servidor responde. tem que manipular para poder usar aqui no cliente
		String line = client.readStringUntil('\r');
		Serial.print(line);
	}

	Serial.println();
	Serial.println("closing connection");

}


unsigned char ToByte(bool b[8])
{
	char c;
	return c;
}

void FromByte(unsigned char c, bool b[8])
{
	
}



